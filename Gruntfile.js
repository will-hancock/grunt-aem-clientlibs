/*
 * grunt-aem-clientlibs
 * https://bitbucket.org/will-hancock/grunt-aem-clientlibs/
 *
 * Copyright (c) 2015 Will Hancock
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

	// Project configuration.
	grunt.initConfig({

		JCR_ROOT_PATH: 'test-aem-package/src/main/content/jcr_root',
		OUTPUT_DIR: 'tests-specs',

		jshint: {
			all: [
				'Gruntfile.js',
				'tasks/*.js',
				'<%= nodeunit.tests %>'
			],
			options: {
				jshintrc: '.jshintrc'
			}
		},

		// Before generating any new files, remove any previously-created files.
		clean: {
			tests: ['<%= OUTPUT_DIR %>']
		},

		// Configuration to be run (and then tested).
		aem_clientlibs: {
			tests_specs: {
				options: {},
				src: [
					'<%= JCR_ROOT_PATH %>/etc/designs/test-website/libs',
					'<%= JCR_ROOT_PATH %>/etc/designs/test-website/libs.tests'
				],
				dest: '<%= OUTPUT_DIR %>'
			}
		},

		// Unit tests.
		nodeunit: {
			tests: ['test/*_test.js']
		}

	});

	// Actually load this plugin's task(s).
	grunt.loadTasks('tasks');

	// These plugins provide necessary tasks.
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-nodeunit');

	// Whenever the "test" task is run, first clean the "tmp" dir, then run this
	// plugin's task(s), then test the result.
	grunt.registerTask('test', ['clean', 'aem_clientlibs', 'nodeunit']);

	// By default, lint and run all tests.
	grunt.registerTask('default', ['jshint', 'test']);

};
