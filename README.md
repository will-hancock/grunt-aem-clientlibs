# grunt-aem-clientlibs

> A Grunt plugin to read AEM clientlibs js.txt and css.txt files out JSON object of filepaths or a concatenated file of all the files ready for use with other grunt plugins. e.g. concat, less compilation, jslint, jasmine unit tests

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-aem-clientlibs --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-aem-clientlibs');
```

## The "aem_clientlibs" task

### Overview
In your project's Gruntfile, add a section named `aem_clientlibs` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  aem_clientlibs: {
    options: {
      // Task-specific options go here.
    },
    your_target: {
      // Target-specific file lists and/or options go here.
    },
  },
});
```

### Options

No options yet... I'm sure as this grows it will become configurable.

### Usage Examples

```js
// GET AEM CLIENT LIB files
aem_clientlibs: {
	tests: {
		src: [
			'<%= JCR_ROOT_PATH %>/etc/designs/test-website/libs',
			'<%= JCR_ROOT_PATH %>/etc/designs/test-website/libs.tests'
		],
		dest: 'tmp/tests'
	}
}
```

The above example will output 4 files into the ```tmp/tests``` dir;

```shell
libs.json
libs.js
libs.tests.json
libs.tests.js
```

libs/js.txt
```shell
#base=scripts

core/app.js
core/example.js
```

Outputs;
libs.json
```json
{
    "files": [
        "test-aem-package/src/main/content/jcr_root/etc/designs/test-website/libs/scripts/core/app.js",
        "test-aem-package/src/main/content/jcr_root/etc/designs/test-website/libs/scripts/core/example.js"
    ]
}
```

libs.js
```js
var app = app || {};
app = (function () {
	var isInitialised = true;
	return {
		init: isInitialised
	}
})();

app.example = (function () {
	var message = 'Hello World!';

	return {
		sayHi: function () {
			return message
		}
	}
})();
```

## Contributing
In lieu of a formal style guide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_