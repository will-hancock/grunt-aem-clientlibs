/*
 * grunt-aem-clientlibs
 * https://bitbucket.org/will-hancock/grunt-aem-clientlibs/
 *
 * Copyright (c) 2015 Will Hancock
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

	// Please see the Grunt documentation for more information regarding task
	// creation: http://gruntjs.com/creating-tasks

	grunt.registerMultiTask('aem_clientlibs', 'A Grunt plugin to read AEM clientlibs js.txt and css.txt files to JSON for use with other grunt plugins. e.g. concat, less compilation, jslint, jasmine unit tests', function () {
		// Merge task-specific and/or target-specific options with these defaults.
		var options = this.options({
				jsExt: 'js.txt',
				cssExt: 'css.txt'
			}),

			// Contructor or a js||css.txt
			TxtFile = function (path, ext) {
				var content = grunt.file.read(path + '/' + ext);
				var lines = [];
				var files = [];
				var base;
				var name;

				function getGruntRelativePath(filepath) {
					var letsWorkBack = function () {
						var _base = base,
							dirs = filepath.split('/');

						dirs = dirs.filter(function (dir, index) {	
							if (dir === '..') {
								_base = _base.substr(0, _base.lastIndexOf('/'));
								return false;
							} else {
								return true;
							}
						});

						return _base + '/' + dirs.join('/');
					};

					if (filepath.indexOf('../') < 0) {
						return base + '/' + filepath;
					} else {
						return letsWorkBack();
					}
				}

				function setFiles() {

					// Filter lines to return a valid file list
					var _files = lines

						// filter out base and comments
						.filter(function (filePath, i) {
							var isBase = base && i === 0,
								isComment = filePath.charAt(0) === '#';

							// remove base and comments if in lines
							if (!(isBase || isComment)) {
								return true;
							} else {
								return false;
							}
						})

						// Replace rows with full file paths
						.map(function (filePath) {
							return getGruntRelativePath(filePath);
						})

						// filter out un-found files
						.filter(function (filePath) {
							var fileExists = grunt.file.exists(filePath);

							if (fileExists) {
								return true;
							} else {
								// warn to unfound files.
								grunt.log.warn('Included file "' + filePath + '" not found.');
								return false;
							}
						});

					return _files;
				}

				function setLines() {
					var _lines = [];
					// Remove line breaks and get array 
					// - nb Windows uses \r\n so normalise to \n
					_lines = content.replace(/\r\n/g,'\n').split('\n');
					// filter out unwanted lines
					_lines = _lines.filter(function (line) {
						if (line.trim().length > 0){
							return true;
						}
						return false;
					});
					return _lines;
				}

				function setBase() {
					var _base, firstLine = lines[0];

					if (firstLine.charAt(0) === '#') {
						_base = firstLine.substr(6); // expecting #base={path}
						return path + '/' + _base;
					}
					return null;
				}

				name = path.substr(path.lastIndexOf('/') + 1);

				lines = setLines();
				base = setBase();
				files = setFiles();

				return {
					name: name,
					path : path,
					content : content,
					lines: lines,
					base: base,
					fileList: {
						files: files
					}
				};

			};

		// Iterate over all specified file groups.
		this.files.forEach(function (f) {

			var src = f.src.filter(function (filepath) {
				// Warn on and remove invalid source files (if nonull was set).
				if (!grunt.file.exists(filepath + '/' + options.jsExt)) {
					grunt.log.warn('Source file "' + filepath + '/' + options.jsExt + '" not found.');
					return false;
				} else {
					return true;
				}
			}).map(function (filepath) {
				// Read file source.
				return new TxtFile(filepath, options.jsExt);
			});

			// OUTPUT .txt output its contents, as a JSON manifest and a concat file
			src.forEach(function (file) {
				var fJSON = JSON.stringify(file.fileList, null, 4),
					concatd;

				// write filelist as json
				grunt.file.write(f.dest + '/' + file.name + '.json', fJSON);
				grunt.log.writeln('File "' + f.dest + '/' + file.name + '.json created.');

				// concat files and write
				concatd = file.fileList.files.map(function (filepath) {
					var fileExists = grunt.file.exists(filepath);
					if (fileExists) {
						return grunt.file.read(filepath);
					} else {
						grunt.log.warn('Source file "' + filepath + '" not found.');
						return '';
					}
					
				}).join(grunt.util.normalizelf(';\n\n'));

				grunt.file.write(f.dest + '/' + file.name + '.js', concatd);
				grunt.log.writeln('File "' + f.dest + '/' + file.name + '.js created.');
			});

			// IF NO SRC
			if (src.length === 0) {
				grunt.log.warn('Source files not found in:', f.orig.src);
			}
			
		});
	});

};
