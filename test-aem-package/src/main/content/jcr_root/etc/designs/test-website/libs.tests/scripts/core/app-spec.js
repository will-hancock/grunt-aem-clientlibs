describe('App tests', function () {
	'use strict';

    it('should pass if app object is defined', function() {
        expect(app).toBeDefined();
    });

    it('should pass if app is initialised', function () {
    	expect(app.init).toBe(true);
    });

});