describe('Hello World', function () {
	'use strict';

    it('should pass if example object is defined', function() {
        expect(app.example).toBeDefined();
    });

    it('should pass if hello responds with message', function () {
    	var message = app.example.sayHi();
    	expect(message).toBe('Hello World!');
    });

});