var app = app || {};
app = (function () {
	var isInitialised = true;
	return {
		init: isInitialised
	}
})();