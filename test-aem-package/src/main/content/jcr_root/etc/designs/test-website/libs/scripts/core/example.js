var app = app || {};

app.example = (function () {
	var message = 'Hello World!';

	return {
		sayHi: function () {
			return message
		}
	}
})();