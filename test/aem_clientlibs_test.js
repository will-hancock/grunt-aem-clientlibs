'use strict';

var grunt = require('grunt');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

exports.aem_clientlibs = {
  setUp: function(done) {
    // setup here if necessary
    done();
  },
  default_options: function(test) {
  	test.expect(4);

  	var fileList = [
		'libs.js',
		'libs.json',
		'libs.tests.js',
		'libs.tests.json'
  	];

  	fileList.forEach(function (file) {
  		var actual = grunt.file.read('tests-specs/' + file);
  		var expected = grunt.file.read('test/expected/' + file);
  		test.equal(actual, expected, 'should describe what the default behavior is.');
  	});

    

    test.done();
  }
};
